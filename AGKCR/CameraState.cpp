#include "stdafx.h"
#include "includes.h"

CameraState::CameraState()
{
	// Domyslne ustawienia kamery:
	Position = glm::vec3(-100.0f, 300.0f, -100.0f);
	Direction = glm::vec3(0.5f, -0.5f, 0.5f);
	Up = glm::vec3(0, 1, 0);

	MovementSpeed = 15.0f;
	RotationSpeed = 1.0f;

	UseMouse = true;

	previousMouseX = 0;
	previousMouseY = 0;
}

CameraState::~CameraState()
{
}

void CameraState::Update(float dt)
{
	GLFWwindow* window = Application::Instance()->window;

	// Obliczenie przesuniecia kursora myszy:
	double mouseX, mouseY;
	glfwGetCursorPos(window, &mouseX, &mouseY);
	double dMouseX = mouseX - previousMouseX;
	double dMouseY = mouseY - previousMouseY;
	previousMouseX = mouseX;
	previousMouseY = mouseY;

	// Modyfikatory szybkosci przemieszczania sie (SHIFT/ALT):
	float speedModifier = 1.0f;
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT))
	{
		speedModifier = 5.0f;
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_ALT))
	{
		speedModifier = .25f;
	}

	// Ruch do przodu/tylu:
	{
		if (glfwGetKey(window, GLFW_KEY_W))
		{
			Position = Position + Direction * MovementSpeed * dt * speedModifier;
		}
		if (glfwGetKey(window, GLFW_KEY_S))
		{
			Position = Position - Direction * MovementSpeed * dt * speedModifier;
		}
	}

	// Ruch na boki:
	{
		glm::vec3 cameraPerp = glm::vec3(Direction.z, 0, -Direction.x); // Kierunek prostopadly do kierunku patrzenia
		cameraPerp = glm::normalize(cameraPerp);
		if (glfwGetKey(window, GLFW_KEY_A))
		{
			Position = Position + cameraPerp * MovementSpeed * dt * speedModifier;
		}
		if (glfwGetKey(window, GLFW_KEY_D))
		{
			Position = Position - cameraPerp * MovementSpeed * dt * speedModifier;
		}
	}


	// Obrot:
	{
		float T = acos(Direction.y); // Aktualny kat obrotu gora/dol [rad]
		float G = atan2(Direction.z, Direction.x); // Aktualny kat obrotu lewo/prawo [rad]

		// Uwzglednienie przesuniecia kursora myszy:
		if (UseMouse)
		{
			G += .1f * dMouseX * RotationSpeed * dt;
			T += .1f * dMouseY * RotationSpeed * dt;
		}

		// Uwzglednienie wcisnietych klawiszy:
		if (glfwGetKey(window, GLFW_KEY_Q))
		{
			G -= RotationSpeed * dt * speedModifier;
		}
		if (glfwGetKey(window, GLFW_KEY_E))
		{
			G += RotationSpeed * dt * speedModifier;
		}
		if (glfwGetKey(window, GLFW_KEY_F))
		{
			T -= RotationSpeed * dt * speedModifier;
		}
		if (glfwGetKey(window, GLFW_KEY_C))
		{
			T += RotationSpeed * dt * speedModifier;
		}

		T = __max(.01f * glm::pi<float>(), __min(.99f * glm::pi<float>(), T)); // Ograniczenie spojrzenia w dol/gore

		// Obliczenie zaktualizowanego, jednostkowego wektora kierunku po obrocie:
		Direction.x = sin(T) * cos(G);
		Direction.y = cos(T);
		Direction.z = sin(T) * sin(G);
	}

	// Ruch w pionie:
	{
		if (glfwGetKey(window, GLFW_KEY_G))
		{
			Position.y = Position.y + MovementSpeed * dt * speedModifier;
		}
		if (glfwGetKey(window, GLFW_KEY_V))
		{
			Position.y = Position.y - MovementSpeed * dt * speedModifier;
		}
	}

}
