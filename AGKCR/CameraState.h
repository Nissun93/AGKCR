#pragma once
class CameraState
{
public:
	CameraState();
	~CameraState();

	void Update(float dt);

	glm::vec3 Position;
	glm::vec3 Direction;
	glm::vec3 Up;
	float MovementSpeed;
	float RotationSpeed;

	bool UseMouse;

private:
	double previousMouseX;
	double previousMouseY;
};

