#pragma once
class Shader
{
public:
	Shader();
	~Shader();

	GLuint ProgramId;

	GLuint UniformLoc_modelview;
	GLuint UniformLoc_projection;
	GLuint UniformLoc_material;
	struct Light {
		GLuint position;
		GLuint ambient;
		GLuint specular;
		GLuint diffuse;
	};
	Light UniformLoc_lights;
	GLuint UniformLoc_tex;
	GLuint UniformLoc_t;
	GLuint UniformLoc_viewerPosition;
	
	// Teksturowanie
	// More IDs
	GLuint texBufferID;
	GLuint texID;
	GLuint texBufferID2;
	GLuint texID2;	

	GLuint AttribLoc_vPosition;
	GLuint AttribLoc_vNormal;
	GLuint AttribLoc_vTexture;
};

