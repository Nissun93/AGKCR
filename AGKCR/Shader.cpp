#include "stdafx.h"
#include "includes.h"

Shader::Shader()
{
	// Zmienne na potrzeby sprawdzenia poprawnosci kompilacji i linkowania:
	GLint success = 0;
	int logLength = 512;
	char* log = new char[logLength];

#pragma region Vertex Shader

	// Kod vertex shadera:
	const char* vsSource = R"(
		#version 330
		
		in vec3 vPosition;
		in vec3 vNormal;
		in vec2 vTexture;

		out vec3 fWorldPosition;
		out vec3 fragNormal;
		out vec2 fTexture;

		uniform mat4 modelview;
		uniform mat4 projection;

		uniform float T;
		
		void main()
		{
			fragNormal = vNormal;
			fTexture = vTexture;

			mat4 mvp = projection * modelview;
			gl_Position = mvp * vec4(vPosition, 1.0);

			fWorldPosition = vPosition;
		}
	)";

	GLuint vs = glCreateShader(GL_VERTEX_SHADER); // Pozyskanie id nowego shadera
	glShaderSource(vs, 1, &vsSource, NULL); // Wyslanie kodu zrodlowego
	glCompileShader(vs); // Kompilacja shadera

	// Sprawdzenie poprawnosci kompilacji:
	glGetShaderiv(vs, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(vs, logLength, NULL, log);
		std::cout << "Vertex Shader compilation error:" << std::endl << log;
		exit(EXIT_FAILURE);
	}

#pragma endregion

#pragma region Fragment Shader

	// Kod fragment shadera:
	const char* fsSource = R"(
		#version 330

										in vec3 fWorldPosition;
		in vec3 fragNormal;
		in vec2 fTexture;

										uniform sampler2DArray tex;
		uniform mat4 modelview;
		uniform vec3 viewerPosition;
		uniform struct uLight {
			vec3 position;
			vec3 ambient;
			vec3 specular;
			vec3 diffuse;
		} Light;

										out vec4 fragColor;
		
		void main()
		{
			vec3 v = normalize(viewerPosition - fWorldPosition);
			float distanceToLight = length(Light.position - fWorldPosition);
			vec3 l = normalize(Light.position - fWorldPosition);
			vec3 r = 2 * dot(fragNormal, l) * fragNormal - l;
			float range1 = 0.15;
			float range2 = 0.85;

			vec3 material = vec3(1, 1, 1);
			vec4 surfaceColor;
			//ska�y
			if (fragNormal.y < range1)
				surfaceColor = texture(tex, vec3(fTexture, 1).rgb * 6);
			//mi�kkie przej�cie mi�dzy teksturami
			else if (fragNormal.y >= range1 && fragNormal.y < range2)
			{
				surfaceColor = texture(tex, vec3(fTexture, 1).rgb * 6) * (1 - fragNormal.y);
				surfaceColor += texture(tex, vec3(fTexture, 0).rgb * 6) * (fragNormal.y);
			}
			//trawa
			else
				surfaceColor = texture(tex, vec3(fTexture, 0).rgb * 6);
			vec3 color = surfaceColor.rgb;
			float attenuation = 1.0 / (1.0 + 0.00001 * pow(distanceToLight, 2));
			vec3 ambient = Light.ambient * material;
			vec3 diffuse = Light.diffuse * material * dot(l, fragNormal);
			vec3 specular = Light.specular * material * pow(dot(v, r), 100);

			color = color + ambient + attenuation * (diffuse + specular);
			
			fragColor = vec4(color, 1);	
			//fragColor = texture(tex, fTexture);
		}
	)";

	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fsSource, NULL);
	glCompileShader(fs);

	// Sprawdzenie poprawnosci kompilacji:
	glGetShaderiv(fs, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fs, logLength, NULL, log);
		std::cout << "Fragment Shader compilation error:" << std::endl << log;
		exit(EXIT_FAILURE);
	}

#pragma endregion

#pragma region Program

	ProgramId = glCreateProgram();
	glAttachShader(ProgramId, vs);
	glAttachShader(ProgramId, fs);
	glLinkProgram(ProgramId);

	// Sprawdzenie poprawnosci linkowania:
	glGetProgramiv(ProgramId, GL_LINK_STATUS, &success);
	if (!success)
	{
		glGetProgramInfoLog(ProgramId, logLength, NULL, log);
		std::cout << "Shading program linking error:" << std::endl << log;
		exit(EXIT_FAILURE);
	}

	// Pozyskanie identyfikatorow zmiennych uniform:
	UniformLoc_modelview = glGetUniformLocation(ProgramId, "modelview");
	UniformLoc_projection = glGetUniformLocation(ProgramId, "projection");
	UniformLoc_lights.position = glGetUniformLocation(ProgramId, "Light.position");
	UniformLoc_lights.ambient = glGetUniformLocation(ProgramId, "Light.ambient");
	UniformLoc_lights.specular = glGetUniformLocation(ProgramId, "Light.specular");
	UniformLoc_lights.diffuse = glGetUniformLocation(ProgramId, "Light.diffuse");
	UniformLoc_viewerPosition = glGetUniformLocation(ProgramId, "viewerPosition");

	// Pozyskanie identyfikatorow atrybutow wierzcholkow:
	AttribLoc_vPosition = glGetAttribLocation(ProgramId, "vPosition");
	AttribLoc_vTexture = glGetAttribLocation(ProgramId, "vTexture");
	AttribLoc_vNormal = glGetAttribLocation(ProgramId, "vNormal");

#pragma endregion
}

Shader::~Shader()
{
}
