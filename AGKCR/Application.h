#pragma once
class Shader;
class CameraState;
class Terrain;

class Application
{
private:
	static Application* instance;
	Application();
public:
	~Application();
	static void Start(int argc, char** argv);
	static Application* Instance();

	static void onError(int error, const char* description);
	static void onKeyPress(GLFWwindow* window, int key, int scancode, int action, int mods);
	static void onSizeChanged(GLFWwindow * window, int w, int h);

	void Initialize();
	void MainLoop();
	void Update(float dt);
	void RenderFrame(double dt);

	GLFWwindow* window;

	glm::mat4 projectionMatrix;
	glm::mat4 viewMatrix;

	struct Lights {
		glm::vec3 position;
		glm::vec3 ambient;
		glm::vec3 specular;
		glm::vec3 diffuse;
	};
	Lights Light;

	glm::vec3 viewerPosition;

	Shader* terrainShader;
	CameraState* camera;
	Terrain* map;

	double currentTime;

	int cArgc;
	char **cArgv;
};

