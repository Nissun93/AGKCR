#pragma once
class Terrain
{
public:
	Terrain(Shader* shader);
	~Terrain();

	void Render();

	GLuint vao;
	GLuint vbo;
	int iboSize;
	unsigned int *ibo;

	int bitmapSize;

	Shader* terrainShader;
};

