#include "stdafx.h"
#include "includes.h"

Terrain::Terrain(Shader* shader)
{
	terrainShader = shader;

	// Wczytanie bitmapy
	Bitmap bitmap = Bitmap("Hills_256.bmp");
	Bitmap texture1 = Bitmap("Textures/GrassDry.bmp");
	Bitmap texture2 = Bitmap("Textures/Rock3.bmp"); 
	bitmapSize = bitmap.height * bitmap.width;
	
	// Tablica pozycji wierzcholkow:
	float *vertices = new float[bitmapSize * 8];

	// Wypelnienie tablicy pozycjami wierzcholkow
	int index;
	int indexBMP;

	for (int raw = 0; raw < bitmap.height; raw++) {
		for (int col = 0; col < bitmap.width; col++) {
			for (int rgb = 0; rgb < 8; rgb++) {
				indexBMP = rgb + col * 3 + raw*bitmap.height * 3;
				index = rgb + col * 8 + raw*bitmap.height * 8;
				//Wypelnianie wierzcholkow
				if (rgb % 8 == 0)
					vertices[index] = raw;
				else if (rgb % 8 == 1)
					vertices[index] = (unsigned char)bitmap.data[indexBMP];
				else if (rgb % 8 == 2)
					vertices[index] = col;
				//Wpisywanie wspolrzednych tekstur
				else if (rgb % 8 == 3) {
					vertices[index] = float(raw) / (bitmap.height - 1);
					//std::cout << "for: " << vertices[index] << std::endl;
				}
				else if (rgb % 8 == 4) {
					vertices[index] = float(col) / (bitmap.width - 1);
					//std::cout << "for: " << vertices[index] << std::endl;
				}
				else
					//continue;
					// Zerowanie wektor�w normalnych i tekstur
					vertices[(index)] = 0;
			}
		}
	}

	// Przygotowanie IBO
	iboSize = bitmapSize + (bitmap.width - 2) * 2 + (bitmap.width)*(bitmap.height - 2);
	Terrain::ibo = new unsigned int[iboSize];
	
	index = 0;
	for (int row = 0; row < bitmap.height - 1; row++) {
		for (int col = 0; col < bitmap.width; col++) {
			ibo[index++] = col + row*bitmap.width;
			ibo[index++] = col + (row + 1)*bitmap.width;
		}
		if (row < bitmap.height - 2)
		{
			ibo[index++] = (row + 2) * bitmap.width - 1;
			ibo[index++] = (row + 1) * bitmap.width;
		}
	}
	// Wektory normalne
	glm::vec3 a;
	glm::vec3 b;
	glm::vec3 c;
	int indexA;
	int indexB;
	int rowA;
	int colA;
	int rowB;
	int colB;

	for (int row = 0; row < bitmap.height; row++) {
		for (int col = 0; col < bitmap.width; col++) {
			index = (col + row * bitmap.height) * 8;
			// index X=5, Y=6, Z=7
			// 1
			if (col > 1 && row < bitmap.height - 1)
			{
				colA = col - 1;
				rowA = row;
				colB = col - 1;
				rowB = row + 1;
				indexA = (colA + rowA * bitmap.height) * 8;
				a = glm::vec3(vertices[indexA] - vertices[index],
					vertices[indexA + 1] - vertices[index + 1],
					vertices[indexA + 2] - vertices[index + 2]);
				indexB = (colB + rowB * bitmap.height) * 8;
				b = glm::vec3(vertices[indexB] - vertices[index],
					vertices[indexB + 1] - vertices[index + 1],
					vertices[indexB + 2] - vertices[index + 2]);
				c = glm::cross(b, a);
				vertices[index + 5] += c.x;
				vertices[index + 6] += c.y;
				vertices[index + 7] += c.z;
			}
			// 2
			if (col > 1 && row < bitmap.height - 1)
			{
				colA = col - 1;
				rowA = row + 1;
				colB = col;
				rowB = row + 1;
				indexA = (colA + rowA * bitmap.height) * 8;
				a = glm::vec3(vertices[indexA] - vertices[index],
					vertices[indexA + 1] - vertices[index + 1],
					vertices[indexA + 2] - vertices[index + 2]);
				indexB = (colB + rowB * bitmap.height) * 8;
				b = glm::vec3(vertices[indexB] - vertices[index],
					vertices[indexB + 1] - vertices[index + 1],
					vertices[indexB + 2] - vertices[index + 2]);
				c = glm::cross(b, a);
				vertices[index + 5] += c.x;
				vertices[index + 6] += c.y;
				vertices[index + 7] += c.z;
			}
			// 3
			if (col < bitmap.width - 1 && row < bitmap.height - 1)
			{
				colA = col;
				rowA = row + 1;
				colB = col + 1;
				rowB = row + 1;
				indexA = (colA + rowA * bitmap.height) * 8;
				a = glm::vec3(vertices[indexA] - vertices[index],
					vertices[indexA + 1] - vertices[index + 1],
					vertices[indexA + 2] - vertices[index + 2]);
				indexB = (colB + rowB * bitmap.height) * 8;
				b = glm::vec3(vertices[indexB] - vertices[index],
					vertices[indexB + 1] - vertices[index + 1],
					vertices[indexB + 2] - vertices[index + 2]);
				c = glm::cross(b, a);
				vertices[index + 5] += c.x;
				vertices[index + 6] += c.y;
				vertices[index + 7] += c.z;
			}
			// 4
			if (col < bitmap.width - 1 && row < bitmap.height - 1)
			{
				colA = col + 1;
				rowA = row + 1;
				colB = col + 1;
				rowB = row;
				indexA = (colA + rowA * bitmap.height) * 8;
				a = glm::vec3(vertices[indexA] - vertices[index],
					vertices[indexA + 1] - vertices[index + 1],
					vertices[indexA + 2] - vertices[index + 2]);
				indexB = (colB + rowB * bitmap.height) * 8;
				b = glm::vec3(vertices[indexB] - vertices[index],
					vertices[indexB + 1] - vertices[index + 1],
					vertices[indexB + 2] - vertices[index + 2]);
				c = glm::cross(b, a);
				vertices[index + 5] += c.x;
				vertices[index + 6] += c.y;
				vertices[index + 7] += c.z;
			}
			// 5
			if (col < bitmap.width - 1 && row > 1)
			{
				colA = col + 1;
				rowA = row;
				colB = col + 1;
				rowB = row - 1;
				indexA = (colA + rowA * bitmap.height) * 8;
				a = glm::vec3(vertices[indexA] - vertices[index],
					vertices[indexA + 1] - vertices[index + 1],
					vertices[indexA + 2] - vertices[index + 2]);
				indexB = (colB + rowB * bitmap.height) * 8;
				b = glm::vec3(vertices[indexB] - vertices[index],
					vertices[indexB + 1] - vertices[index + 1],
					vertices[indexB + 2] - vertices[index + 2]);
				c = glm::cross(b, a);
				vertices[index + 5] += c.x;
				vertices[index + 6] += c.y;
				vertices[index + 7] += c.z;
			}
			// 6
			if (col < bitmap.width - 1 && row > 1)
			{
				colA = col + 1;
				rowA = row - 1;
				colB = col;
				rowB = row - 1;
				indexA = (colA + rowA * bitmap.height) * 8;
				a = glm::vec3(vertices[indexA] - vertices[index],
					vertices[indexA + 1] - vertices[index + 1],
					vertices[indexA + 2] - vertices[index + 2]);
				indexB = (colB + rowB * bitmap.height) * 8;
				b = glm::vec3(vertices[indexB] - vertices[index],
					vertices[indexB + 1] - vertices[index + 1],
					vertices[indexB + 2] - vertices[index + 2]);
				c = glm::cross(b, a);
				vertices[index + 5] += c.x;
				vertices[index + 6] += c.y;
				vertices[index + 7] += c.z;
			}
			// 7
			if (col > 1 && row > 1)
			{
				colA = col;
				rowA = row - 1;
				colB = col - 1;
				rowB = row - 1;
				indexA = (colA + rowA * bitmap.height) * 8;
				a = glm::vec3(vertices[indexA] - vertices[index],
					vertices[indexA + 1] - vertices[index + 1],
					vertices[indexA + 2] - vertices[index + 2]);
				indexB = (colB + rowB * bitmap.height) * 8;
				b = glm::vec3(vertices[indexB] - vertices[index],
					vertices[indexB + 1] - vertices[index + 1],
					vertices[indexB + 2] - vertices[index + 2]);
				c = glm::cross(b, a);
				vertices[index + 5] += c.x;
				vertices[index + 6] += c.y;
				vertices[index + 7] += c.z;
			}
			// 8
			if (col > 1 && row > 1)
			{
				colA = col - 1;
				rowA = row - 1;
				colB = col - 1;
				rowB = row;
				indexA = (colA + rowA * bitmap.height) * 8;
				a = glm::vec3(vertices[indexA] - vertices[index],
					vertices[indexA + 1] - vertices[index + 1],
					vertices[indexA + 2] - vertices[index + 2]);
				indexB = (colB + rowB * bitmap.height) * 8;
				b = glm::vec3(vertices[indexB] - vertices[index],
					vertices[indexB + 1] - vertices[index + 1],
					vertices[indexB + 2] - vertices[index + 2]);
				c = glm::cross(b, a);
				vertices[index + 5] += c.x;
				vertices[index + 6] += c.y;
				vertices[index + 7] += c.z;
			}
			c = glm::vec3(vertices[index + 5], vertices[index + 6], vertices[index + 7]);
			c = glm::normalize(c);
			vertices[index + 5] = c.x;
			vertices[index + 6] = c.y;
			vertices[index + 7] = c.z;
		}
	}

	// Wygenerowanie id i aktywacja nowego VAO:
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// Wygenerowanie id i aktywacja nowego VBO:
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	// Wypelnienie VBO zawartoscia:
	glBufferData(GL_ARRAY_BUFFER, bitmapSize * 8 * sizeof(float), vertices, GL_STATIC_DRAW);
	// Od tego momentu nie trzeba juz trzymac wierzcholkow w pamieci glownej, znajduja sie juz w pamieci karty graficznej

	// Wlaczenie atrybutu wierzcholka (pozycja):
	glEnableVertexAttribArray(terrainShader->AttribLoc_vPosition);

	// Wskazanie gdzie w VBO znajduja sie wartosci atrybutu wierzcholka (pozycja):
	glVertexAttribPointer(terrainShader->AttribLoc_vPosition, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (const GLvoid*)(0 * sizeof(GLfloat)));
	
	int texturesNumber = 2;
	glGenTextures(1, &terrainShader->texBufferID);
	glBindTexture(GL_TEXTURE_2D_ARRAY, terrainShader->texBufferID);
	glTexParameterf(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	glTexParameterf(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	glTexParameterf(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, GL_SRGB8, 512, 512, texturesNumber, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, texture1.width, texture1.height, 1, GL_RGB, GL_UNSIGNED_BYTE, texture1.data);
	glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 1, texture2.width, texture2.height, 1, GL_RGB, GL_UNSIGNED_BYTE, texture2.data);

	glGenerateMipmap(GL_TEXTURE_2D_ARRAY);


	// Wlaczenie atrybutu wierzcholka (tekstura):
	glEnableVertexAttribArray(terrainShader->AttribLoc_vTexture);

	// Wskazanie gdzie w VBO znajduja sie wartosci atrybutu wierzcholka (tekstura):
	glVertexAttribPointer(terrainShader->AttribLoc_vTexture, 2, GL_FLOAT, GL_TRUE, 8 * sizeof(float), (const GLvoid*)(3 * sizeof(GLfloat)));

	
	// Wlaczenie atrybutu wierzcholka (wektor normalny):
	glEnableVertexAttribArray(terrainShader->AttribLoc_vNormal);

	// Wskazanie gdzie w VBO znajduja sie wartosci atrybutu wierzcholka (wektor normalny):
	glVertexAttribPointer(terrainShader->AttribLoc_vNormal, 3, GL_FLOAT, GL_TRUE, 8 * sizeof(float), (const GLvoid*)(5 * sizeof(GLfloat)));

// Wylaczenie VAO (nie jest niezbedne):
	glBindVertexArray(0);
}

Terrain::~Terrain()
{
}

void Terrain::Render()
{
	// Wybor programu cieniujacego:
	glUseProgram(terrainShader->ProgramId);

	// Aktywacja VAO:
	glBindVertexArray(vao);

	// Ustawienie aktualnych wartosci zmiennych uniform (macierze modelu-widoku oraz projekcji):
	glUniformMatrix4fv(terrainShader->UniformLoc_modelview, 1, GL_FALSE, glm::value_ptr(Application::Instance()->viewMatrix));
	glUniformMatrix4fv(terrainShader->UniformLoc_projection, 1, GL_FALSE, glm::value_ptr(Application::Instance()->projectionMatrix));
	glUniform3fv(terrainShader->UniformLoc_lights.position, 1, glm::value_ptr(Application::Instance()->Light.position));
	glUniform3fv(terrainShader->UniformLoc_lights.ambient, 1, glm::value_ptr(Application::Instance()->Light.ambient));
	glUniform3fv(terrainShader->UniformLoc_lights.specular, 1, glm::value_ptr(Application::Instance()->Light.specular));
	glUniform3fv(terrainShader->UniformLoc_lights.diffuse, 1, glm::value_ptr(Application::Instance()->Light.diffuse));
	glUniform3fv(terrainShader->UniformLoc_viewerPosition, 1, glm::value_ptr(Application::Instance()->viewerPosition));
	
	// Zadanie renderowania wierzcholkow z aktualnie aktywnego VBO (wskazanego przez aktywne VAO), w kolejnosci takiej w jakiej znajduja sie w VBO:
	glDrawElements(GL_TRIANGLE_STRIP, iboSize, GL_UNSIGNED_INT, ibo);
	//glDrawArrays(GL_TRIANGLE_STRIP, 0, bitmapSize);

	// Sprzatanie (nie jest niezbedne):
	glBindVertexArray(0);
	glUseProgram(0);
}
