#include "stdafx.h"
#include "includes.h"

using namespace std;

#pragma region Singleton

Application* Application::instance = NULL;

Application::Application()
{
}

Application::~Application()
{
}

Application* Application::Instance()
{
	if (instance == NULL)
	{
		cout << "Application not initialized.";
		exit(EXIT_FAILURE);
	}
	return instance;
}

void Application::Start(int argc, char** argv)
{
	//-------------------
	//cout << "colours: " << endl;
	//cout << bitmap->colours << endl;
	//-------------------
	instance = new Application();
	instance->cArgc = argc;
	instance->cArgv = argv;
	instance->Initialize();
}

#pragma endregion

#pragma region Inicjalizacja

void Application::Initialize()
{
	glfwSetErrorCallback(onError); // Ustawienie obslugi bledow GLFW przed jego inicjalizacja

	// Inicjalizacja GLFW:
	if (!glfwInit())
	{
		exit(EXIT_FAILURE);
	}

	// Oczekiwana wersja OpenGL:
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	glfwWindowHint(GLFW_SAMPLES, 4); // Antialiasing (MSAA)

	// Utworzenie okna:
	window = glfwCreateWindow(800, 600, "AGKCR", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window); // Wybor (aktywacja) kontekstu OpenGL
	glewInit(); // Inicjalizacja GLEW

	// Wyswietlenie w konsoli danych karty graficznej i wybranej wersji kontekstu OpenGL:
	const GLubyte* renderer = glGetString(GL_RENDERER);
	const GLubyte* version = glGetString(GL_VERSION);
	cout << "Device: " << renderer << endl;
	cout << "OpenGL version: " << version << endl << endl;

	glfwSwapInterval(1); // Vertical Sync (0 = off, 1 = on)

	// Ustawienie obslugi eventow GLFW:
	glfwSetKeyCallback(window, onKeyPress);
	glfwSetFramebufferSizeCallback(window, onSizeChanged);

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); // Kursor zostaje przechwycony przez okno (znika i nie wyjedzie poza jego obszar)

	// Reczne wywolanie onSizeChanged po raz pierwszy - GLFW nie robi tego sam po utworzeniu okna:
	int width, height;
	glfwGetFramebufferSize(window, &width, &height);
	onSizeChanged(window, width, height);

	// Ustawienie cech stanu OpenGL:
	glEnable(GL_DEPTH_TEST);
	glClearColor(.36f, .58f, .88f, 1.0f);

	// Utworzenie elementow sceny:
	Light.position = glm::vec3(255.0f, 50.0f, 0.0f);
	Light.ambient = glm::vec3(0.2f, 0.2f, 0.2f);
	Light.specular = glm::vec3(1.0f, 1.0f, 1.0f);
	Light.diffuse = glm::vec3(1.0f, 1.0f, 1.0f);
	terrainShader = new Shader();
	camera = new CameraState();
	map = new Terrain(terrainShader);
	MainLoop(); // Wejscie do petli glownej

	glfwTerminate(); // Zwolnienie zasobow GLFW
}

#pragma endregion

#pragma region Petla glowna programu

// Petla glowna (zmienny krok czasowy):
void Application::MainLoop()
{
	double lastT = glfwGetTime();
	while (!glfwWindowShouldClose(window))
	{
		//Light.position = camera->Position;
		currentTime = glfwGetTime();
		float dt = currentTime - lastT;
		Update(dt);
		RenderFrame(dt);
		glfwPollEvents();
		lastT = currentTime;
	}
}

// Aktualizacja swiata
void Application::Update(float dt)
{
	camera->Update(dt);
	//lights = camera->Position;
	viewMatrix = glm::lookAt(camera->Position, camera->Position + camera->Direction, camera->Up);
	viewerPosition = camera->Position;
	//std::cout << "camera->Pos = " << camera->Position.x << " | " << camera->Position.y << " | " << camera->Position.z << std::endl;
	//std::cout << "viewMatrix  = " << viewMatrix[0][0] << " | " << viewMatrix[0][1] << " | " << viewMatrix[0][2] << std::endl;
}

// Renderowanie klatki
void Application::RenderFrame(double dt)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	map->Render();

	glfwSwapBuffers(window);
}

#pragma endregion

#pragma region Obsluga zdarzen

// Obsluga bledow GLFW
void Application::onError(int error, const char* description)
{
	fprintf(stderr, "GLFW Error: %s\n", description);
}

// Wcisniecie klawisza
void Application::onKeyPress(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	}
	if (key == GLFW_KEY_M && action == GLFW_PRESS)
	{
		Application::Instance()->camera->UseMouse = !Application::Instance()->camera->UseMouse;
		glfwSetInputMode(window, GLFW_CURSOR, Application::Instance()->camera->UseMouse ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);
	}
}

// Zmiana rozmiaru bufora klatki
void Application::onSizeChanged(GLFWwindow * window, int w, int h)
{
	float aspect = (float)w / h; // Wspolczynnik proporcji dlugosci bokow
	Application::Instance()->projectionMatrix = glm::perspective<float>(glm::radians(50.0f), aspect, .01f, 1000.0f); // Ustawienie macierzy projekcji perspektywicznej
	glViewport(0, 0, w, h); // Okreslenie wymiarow renderowanego viewportu
}

#pragma endregion
